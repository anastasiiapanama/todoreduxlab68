import React, {useEffect, useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import {useDispatch, useSelector} from "react-redux";
import {addTasks, deleteTask, fetchList} from "../store/actions";
import Task from "../../components/Task/Task";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const ToDoList = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const state = useSelector(state => state);

    const [content, setContent] = useState({
        title: '',
        content: ''
    });

    const changeContent = event => {
        const {name, value} = event.target;

        setContent(prev =>({
            ...prev,
            [name]: value
        }));
    };

    useEffect(() => {
       dispatch(fetchList());
    }, [dispatch]);

    const submitTasks = event => {
        event.preventDefault();

        dispatch(addTasks(content))
    };

    const deleteTaskHandler = (e, id) => {
        e.preventDefault();

        dispatch(deleteTask(id));
    };

    return (
        <>
            <Paper className={classes.paper}>
                <form onSubmit={submitTasks}>
                    <Grid container direction="column" spacing={3}>
                        <Grid item xs>
                            <Typography variant="h5" align="center">Add task</Typography>
                        </Grid>
                        <Grid item xs>
                            <TextField
                                variant="outlined"
                                fullwidth
                                type="text"
                                name="title"
                                label="Title"
                                value={content.title}
                                required
                                onChange={changeContent}
                            />
                        </Grid>
                        <Grid item xs>
                            <TextField
                                variant="outlined"
                                fullWidth
                                type="content"
                                name="content"
                                label="Content"
                                value={content.content}
                                required
                                onChange={changeContent}
                                multiline
                                rows={5}
                            />
                        </Grid>
                        <Grid item xs>
                            <Button type="submit" variant="contained" color="primary">Add</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            <Paper className={classes.paper}>
                <Grid container direction="column" spacing={2}>
                    <Typography variant="h5" align="center">Task list</Typography>
                    <Grid item xs>
                        {state.tasks.map(task => (
                            <Task
                                key={task.id}
                                title={task.title}
                                content={task.content}
                                delete={e => deleteTaskHandler(e, task.id)}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Paper>
        </>
    );
};

export default ToDoList;