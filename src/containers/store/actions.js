import axiosList from "../../axios-list";

export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';
export const FETCH_TASKS_FAILURE = 'FETCH_TASKS_FAILURE';
export const ADD_TASKS = 'ADD_TASKS';

export const fetchTasksRequest = () => ({type: FETCH_TASKS_REQUEST});
export const fetchTasksSuccess = tasks => ({type: FETCH_TASKS_SUCCESS, tasks});
export const fetchTasksFailure = () => ({type: FETCH_TASKS_FAILURE});
export const addTasksType = () => ({type: ADD_TASKS});


export const fetchList = () => {
    return async dispatch => {
        dispatch(fetchTasksRequest());

        try {
            const response = await axiosList.get('tasks.json');

            const tasks = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));

            dispatch(fetchTasksSuccess(tasks));
        } catch (e) {
            dispatch(fetchTasksFailure());
        }
    }
};

export const addTasks = data => {
    return async dispatch => {
        try {
            await axiosList.post("/tasks.json", data);

            dispatch(addTasksType());
            dispatch(fetchList());
        } catch (e) {
            console.log(e);
        }
    }
};

export const deleteTask = (id) => {
    return async dispatch => {
        try {
            await axiosList.delete("/tasks/" + id + ".json");

            dispatch(fetchList());
        } catch (e) {
            console.log(e);
        }
    }
};

