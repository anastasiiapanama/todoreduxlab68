import {ADD_TASKS, FETCH_TASKS_FAILURE, FETCH_TASKS_REQUEST, FETCH_TASKS_SUCCESS} from "./actions";

const initialState = {
    tasks: [],
    loading: true,
    error: false
};

const reducer = (state= initialState, action) => {
    switch (action.type) {
        case FETCH_TASKS_REQUEST:
            return {...state, loading: true, error: false};
        case FETCH_TASKS_SUCCESS:
            return {...state, loading: false, tasks: action.tasks};
        case FETCH_TASKS_FAILURE:
            return {...state, loading: false, error: true};
        case ADD_TASKS:
            return {...state, loading: true};
        default:
            return state;
    }
};

export default reducer;