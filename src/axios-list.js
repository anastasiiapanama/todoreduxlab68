import axios from "axios";

const axiosList = axios.create({
   baseURL: 'https://propject-burger-ponamareva-default-rtdb.firebaseio.com/'
});

export default axiosList;