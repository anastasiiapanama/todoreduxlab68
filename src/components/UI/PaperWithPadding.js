import React from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Paper} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
    }
}));

const PaperWithPadding = ({children}) => {
    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            {children}
        </Paper>
    );
};

export default PaperWithPadding;