import React from 'react';
import './Task.css';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import PaperWithPadding from "../UI/PaperWithPadding";

const Task = props => {


    return (
        <PaperWithPadding>
            <Grid container direction="raw" spacing={3}>
                <Grid item xs>
                    <Typography variant="h5" align="center">{props.title}</Typography>
                </Grid>
                <Grid item xs>
                    <Typography variant="h6" align="center">{props.content}</Typography>
                </Grid>
                <Grid item xs>
                    <Button type="submit" variant="contained" color="primary" onClick={props.delete}>Delete</Button>
                </Grid>
            </Grid>
        </PaperWithPadding>
    );
};

export default Task;